var PORT = process.env.PORT || 3000;
var JSON_PARSE_ERROR = {
	"error": "Could not decode request: JSON parsing failed"
};
var PAYLOAD_NOT_PRESENT_ERROR = {
	"error": "Could not decode request: Payload not presented"
}
var NOT_FOUND_ERROR = {
	"error": "requested resource could not be found"
};
var express = require('express');
var app = express();

app.use(function (req, res, next) {
	if (req.method == "POST") {
		var _data = "";
		req.on('data', function (chunk) {
			_data += chunk;
		})
		req.on('end', function () {
			req.body = _data;
			next();
		});
	} else {next();};
});

app.post('/', function (req, res) {
	res.setHeader('Content-Type', 'application/json');
	res.statusCode = 400;
	try {
		var body = JSON.parse(req.body);
		///////////////////////
		// check for payload //
		///////////////////////
		if (!body.payload) {
			console.warn("Payload is not presented");
			res.send(PAYLOAD_NOT_PRESENT_ERROR);
			return;
		};

		///////////////////////
		// assemble response //
		///////////////////////
		var _sa = [];	/*_sa for shows array*/
		for (var i = 0; i < body.payload.length; i++) {
			var _s = body.payload[i];	/*_s for show*/
			if (!_s.drm) {continue;};
			if (!_s.episodeCount) {continue;};
			if (_s.episodeCount <= 0) {continue;};
			_sa.push(extractShow(_s));
		};
		res.statusCode = 200;
		res.send({
			response: _sa
		});
		//////////////////
		// end and sent //
		//////////////////
	} catch (e) {
		console.error(e);
		res.send(JSON_PARSE_ERROR);
	};
});

app.all('*', function (req, res) {
	res.setHeader('Content-Type', 'application/json');
	res.statusCode = 404;
	res.send(NOT_FOUND_ERROR);
});

var server = app.listen(PORT, function () {
	console.log("server is up and listening on port " + PORT + "...");
});

/**
 * translate show into required format for response
 * @param  {object} show 	assume it has been filtered and matches requirement
 * @return {object}      	attr: image, slug, title
 */
function extractShow (show) {
	var _r = {}; /*_r for return value*/
	_r.slug = show.slug ? show.slug : null;
	_r.title = show.title ? show.title : null;
	if (!show.image) {_r.image = null}
	else {_r.image = show.image.showImage ? show.image.showImage : null;};
	return _r;
};